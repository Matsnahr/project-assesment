<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--menggunakan bootstrap CDN untuk mendapatkan source-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/2.0.1/css/scroller.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<title>Perpustakaan</title>
</head>
<body>


<!-- Image and text -->
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="#"> 
    <img src="asset/img/logo.png" width="100" height="50" class="d-inline-block align-top" alt="" loading="lazy">
    SMK Muhammadiyah Bandongan
  </a>
</nav>

<div class="container-fluid top-bar">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <center><img src="asset/img/logos.png" width="100" height="100" class="mt-2 mr-4 mb-3"  alt=""></center>
                    <center><h1>Data Buku Perpustakaan</h1></center>
                <div class="col-md-7 col-sm-12 col-xs-12">
                </div>
            </div>
        </div>
    </div>
</div>

<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="4"></li>
    
</ol>
<div class="carousel-inner">
  <div class="carousel-item active">
      <img src="asset/img/vektor.jpg" class="d-block w-100" alt="Gagal">
        <div class="carousel-caption d-none d-md-block">
            <h2>طَلَبُ الْعِلْمِ فَرِيْضَةٌ عَلَى كُلِّ مُسْلِمٍ</h2>
            <p>“Menuntut ilmu itu wajib atas setiap Muslim” (HR. Ibnu Majah no. 224).</p>
        </div>
    </div>
<div class="carousel-item">
    <img src="asset/img/foto2.jpg" class="d-block w-100" alt="Gagal">
        <div class="carousel-caption d-none d-md-block">
            <h2>إِنَّا لِلَّهِ وَإِنَّا إِلَيْهِ رَاجِعُونَ</h2>
            <p>"Sesungguhnya kami adalah milik Allah dan kepada Allah jugalah kami kembali".</p>
        </div>
</div>
<div class="carousel-item">
    <img src="asset/img/foto3.jpg" class="d-block w-100" alt="Gagal">
        <div class="carousel-caption d-none d-md-block">
            <h2>3 Amalan yang Tidak akan Terputus Walaupus sudah Mati</h2>
            <p>“Jika seorang meninggal dunia, maka terputuslah amalannya kecuali tiga perkara (yaitu): sedekah jariyah, ilmu yang dimanfaatkan, atau doa anak yang sholeh”. (HR. Muslim No.1631)</p>
        </div>
</div>
<div class="carousel-item">
    <img src="asset/img/foto4.jpg" class="d-block w-100" alt="Gagal">
        <div class="carousel-caption d-none d-md-block">
            <h2>أَشْهَدُ أَنْ لَا إِلَهَ إِلَّا اللهُ وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ اللهِ</h2>
            <p>"Aku bersaksi bahwa tidak ada Tuhan melainkan Allah. Dan aku bersaksi bahwa Nabi Muhammad adalah utusan Allah".</p>
        </div>
</div>
    <div class="carousel-item">
        <img src="asset/img/cbd.jpeg" class="d-block w-100" alt="Gagal">
        <div class="carousel-caption d-none d-md-block">
            <h2><a href="#" class="text-white bg-dark">فَبِأَيِّ آَلَاءِ رَبِّكُمَا تُكَذِّبَانِ </a></h2>
            <p><a href="#" class="text-white bg-dark">"Maka nikmat Tuhan kamu yang manakah yang kamu dustakan"</a></p>
        </div>
    </div>
</div>
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<br>
<div class="container">
    <div class="jumbotron"> <!--memberi efek kotak berwarna abu-abu dengan class jumbotron-->
    <img src="asset/img/logo.png" height="150" alt="gagal" style="display: block; margin: auto;">
    <br>
    <button data-toggle="modal" data-target="#modaltambah"  class="btn btn-danger">Tambah Data Buku</button>

        <!--Modal untuk tambah data buku-->
        <div class="modal fade" id="modaltambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                        <form action="proses.php?proses=tambah&id=" method="post">
                            <div class="formgroup">
                                <label>Nama Buku</label>
                                    <input type="text" name="namabuku" class="form-control" placeholder="Silahkan Masukan Nama Buku">
                            </div>
                                <div class="formgroup">
                                    <label>Jenis Buku</label>
                                    <select name="jenisbuku" class="form-control">
                                        <option value="Sains">Sains</option>
                                        <option value="Fisika">Fisika</option>
                                        <option value="Biologi">Biologi</option>
                                        <option value="Agama">Agama</option>
                                        <option value="Kejuruan">Kejuruan</option>
                                        <option value="Humor">Humor</option>
                                        <option value="Komik">Komik</option>
                                        <option value="Majalah">Majalah</option>
                                        <option value="Novel">Novel</option>
                                    </select>
                                </div>
                                <div class="formgroup">
                                    <label>Pengarang</label>
                                    <input type="text" name="pengarang" class="form-control" placeholder="Silahkan Masukan Nama Pengarang Buku">
                                </div>
                                <div class="formgroup">
                                    <label>Tahun Terbit</label>
                                    <input type="year" name="tahunterbit" class="form-control" placeholder="Silahkan Masukan Tahun Terbit">
                                </div>
                                    <div class="formgroup">
                                        <label>Penerbit</label>
                                        <input type="text" name="penerbit" class="form-control" placeholder="Silahkan Masukan Penerbit">
                                    </div>
                                    <div class="formgroup">
                                        <label>ISBN</label>
                                        <input type="text" name="isbn" class="form-control" placeholder="Silahkan Masukan No ISBN">
                                    </div>
                            </div>
                                <div class="modal-footer">
                                        <button type="submit" class="btn btn-warning">Kirim</button>
                                  
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                    <table class="table table-striped" id="example" style="width:100%"> <!--menggunakan datatable client side-->
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Buku</th>
                                <th>Jenis buku</th>
                                <th>Pengarang</th>
                                <th>Tahun Terbit</th>
                                <th>Penerbit</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

<?php
 include 'koneksi.php';
 $no=1;
 $sql = "SELECT * FROM buku";
 $data = $conn->query($sql);
 foreach($data as $hasil){ ?> 
 <tr>
     <td><?=$no++; ?></td>
     <td><?=$hasil['namabuku'];?></td>
     <td><?=$hasil['jenisbuku'];?></td>
     <td><?=$hasil['pengarang'];?></td>
     <td><?=$hasil['tahunterbit'];?></td>
     <td><?=$hasil['penerbit'];?></td>
 <td>
     <button data-toggle="modal" data-target="#modaledit<?=$hasil['id_buku']; ?>" class="btn btn-warning">Edit</button>
        <div class="modal fade" id="modaledit<?=$hasil['id_buku']; ?>">
            <div class="modal-dialog" role="document">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>             
                         </button>
                     </div>
                     <div class="modal-body">
                     <form action="proses.php?proses=edit&id=<?=$hasil['id_buku'];?>" method="post">
                    <div class="formgroup"> 
                        <label>Nama Buku</label>
                        <input type="text" name="namabuku" value="<?=$hasil['namabuku'];?>" class="form-control" placeholder="Silahkan Masukan Nama Buku">
                    </div>
                    <div class="formgroup">
                        <label>Jenis Buku</label>
                        <select name="jenisbuku" class="form-control">
                            <option <?=$hasil['jenisbuku'] == 'Sains' ? 'selected':''; ?> value="Sains">Sains</option>
                            <option <?=$hasil['jenisbuku'] == 'Fisika' ? 'selected':''; ?> value="Fisika">Fisika</option>
                            <option <?=$hasil['jenisbuku'] == 'Biologi' ? 'selected':''; ?> value="Biologi">Biologi</option>
                            <option <?=$hasil['jenisbuku'] == 'Agama' ? 'selected':''; ?> value="Agama">Agama</option>
                            <option <?=$hasil['jenisbuku'] == 'Kejuruan' ? 'selected':''; ?> value="Kejuruan">Kejuruan</option>
                            <option <?=$hasil['jenisbuku'] == 'Teknik' ? 'selected':''; ?> value="Teknik">Teknik</option>
                            <option <?=$hasil['jenisbuku'] == 'Humor' ? 'selected':''; ?> value="Humor">Humor</option>
                            <option <?=$hasil['jenisbuku'] == 'Komik' ? 'selected':''; ?> value="Komik">Komik</option>
                            <option <?=$hasil['jenisbuku'] == 'Majalah' ? 'selected':''; ?> value="Majalah">Majalah</option>
                            <option <?=$hasil['jenisbuku'] == 'Novel' ? 'selected':''; ?> value="Novel">Novel</option>
                        </select>
                        </div>
                        <div class="formgroup">
                            <label>Pengarang</label>
                            <input type="text" name="pengarang" class="form-control" value="<?=$hasil['pengarang'];?>" placeholder="Silahkan Masukan Nama Pengarang Buku">
                        </div>
                        <div class="formgroup">
                            <label>tahunterbit</label>
                            <input type="text" name="tahunterbit" class="form-control" value="<?=$hasil['tahunterbit'];?>" placeholder="Silahkan Masukan Tahun Terbit">
                        </div>
                        <div class="formgroup">
                            <label>penerbit</label>
                            <input type="text" name="penerbit" class="form-control" value="<?=$hasil['penerbit'];?>" placeholder="Silahkan Masukan Penerbit">
                        </div>
                        <div class="formgroup">
                            <label>ISBN</label>
                            <input type="text" name="ISBN" class="form-control" value="<?=$hasil['ISBN'];?>" placeholder="Silahkan Masukan No ISBN">
                        </div>
                    
                         <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                         </div>
                         </form>
                     </div>
                 </div>
             </div>
         </div>
         <a href="proses.php?proses=hapus&id=<?=$hasil['id_buku'];?>" class="btn btn-danger">Hapus</button>
     </td>
 </tr>
 <?php }?>
    </tbody>
    </table>
 </div>
</div>

<center><p&copy>@copyright2020</p></center>
<script>
        $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/scroller/2.0.1/js/dataTables.scroller.min.js"></script>
</body>
</html>