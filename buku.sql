-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2020 at 11:00 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coba`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(11) NOT NULL,
  `namabuku` varchar(50) NOT NULL,
  `jenisbuku` enum('Sains','Kejuruan','Fisika','Biologi','Agama','Teknik','Humor','Komik','Majalah','Novel') NOT NULL,
  `tahunterbit` year(4) NOT NULL,
  `pengarang` varchar(20) NOT NULL,
  `penerbit` varchar(20) NOT NULL,
  `ISBN` bigint(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id_buku`, `namabuku`, `jenisbuku`, `tahunterbit`, `pengarang`, `penerbit`, `ISBN`) VALUES
(4, 'Janur Kuning VS Bendera Kuning', 'Agama', 2020, 'Matsna', 'matsnahr', 0),
(6, 'Anak Pungut', 'Novel', 2015, 'matsna', 'Sinar Jaya', 0),
(7, 'Cara menginstall Linux di Android', 'Kejuruan', 2020, 'aku', 'iNews', 0),
(8, 'Dasar-dasar Pemrograman', 'Kejuruan', 2015, 'Leonardo', 'iNews', 1214),
(10, 'Farmatologi', 'Kejuruan', 2014, 'Matsna', 'Ayo sehat', 0),
(11, 'Siksa Kubur', 'Agama', 2020, 'aku', 'matsnahr', 0),
(12, 'Kisah Nabi Musa', 'Agama', 2009, 'Makfud', 'al islam', 0),
(13, 'Ekosistem di Bulan', 'Biologi', 2015, 'Budi', 'Nasa', 0),
(14, 'Cara menginstall ulang Karbu', 'Kejuruan', 2020, 'Smk Mutu', 'Mutunews', 0),
(16, 'tetanggaku real cctv', 'Sains', 2020, 'Matsna', 'bukuku', 0),
(17, 'Malam pertama di Kubur', 'Agama', 2014, 'Matsna', 'iNews', 0),
(18, 'Cara bernafas', 'Sains', 2013, 'alexander', 'Gramedia', 0),
(19, 'Fisika XII', 'Fisika', 2007, 'Matsna', 'Gramedia', 0),
(20, 'Ketika Jomblo bertasbih', 'Sains', 2015, 'Matsna', 'Gramedia', 0),
(23, 'Pak Tani  Mencuri Timun', 'Fisika', 2019, 'Matsna', 'Sinar Jaya', 0),
(24, 'Resep Kraby Patty', 'Majalah', 2020, 'Matsna', 'matsnahr', 0),
(25, 'Mahkota untuk Orang Tua', 'Novel', 2019, 'Matsna', 'matsnahr', 0),
(26, 'Amalan Nabi Muhammad SAW', 'Agama', 2010, 'Makfud', 'al islam', 1226756669),
(27, 'Kisah si Corona', 'Komik', 2012, 'Newton', 'Gramedia', 3456726183),
(33, 'Allah dulu Baru Kamu', 'Novel', 2019, 'Matsna', 'matsnahr', 4263421537);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
